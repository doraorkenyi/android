package com.example.quizgamescore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class HomePageActivity extends AppCompatActivity {

    TextView tHistory;
    TextView tGeography;
    TextView tLiterature;
    TextView tOther;
    String userID;
    URL mainURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        tHistory = (TextView) findViewById(R.id.tvHistory);
        tGeography = (TextView) findViewById(R.id.tvGeography);
        tLiterature = (TextView) findViewById(R.id.tvLiterature);
        tOther = (TextView) findViewById(R.id.tvOther);

        Intent intent = getIntent();
        userID = intent.getStringExtra("userId");
        try {
            mainURL = new URL("http://10.0.2.2:4000/quiz/userScore/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void onButtonClick(View view) {
        Thread thread2 = new Thread(() -> {
            try {
                URL url = new URL(mainURL, userID);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Scanner scanner = new Scanner(connection.getInputStream());
                    StringBuilder raw_resp = new StringBuilder();

                    while (scanner.hasNext()) {
                        raw_resp.append(scanner.nextLine());
                    }
                    Log.println(Log.ERROR, "Request Data:", raw_resp.toString());

                    JSONObject jsonObject = new JSONObject(raw_resp.toString());

                    String historyScore = jsonObject.getString("ScoreHistory");
                    String geographyScore = jsonObject.getString("ScoreGeography");
                    String literatureScore = jsonObject.getString("ScoreLiterature");
                    String otherScore = jsonObject.getString("ScoreOther");

                    String point = "points";
                    String history = "History Score: ";
                    String geography = "Geography Score: ";
                    String literature = "Literature Score: ";
                    String other = "Other Score: ";
                    String setHistory = history + historyScore + point;
                    String setGeography = geography + geographyScore + point;
                    String setLiterature = literature + literatureScore + point;
                    String setOther = other + otherScore + point;

                    runOnUiThread(() -> {
                        tHistory.setText(setHistory);
                        tGeography.setText(setGeography);
                        tLiterature.setText(setLiterature);
                        tOther.setText(setOther);
                    });
                }
            } catch (MalformedURLException e) {
                Log.println(Log.ERROR, "URL", "Wrong URL");
                e.printStackTrace();
            } catch (IOException e) {
                Log.println(Log.ERROR, "Connection", "Connection issue");
                e.printStackTrace();
            } catch (JSONException e) {
                Log.println(Log.ERROR, "JSON", "Malformed JSON");
                e.printStackTrace();
            }
        });

        thread2.start();
    }
}